<?php

namespace App\Http\Controllers\Curl;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\TransferStats;
use Illuminate\Http\Request;

class CurlRequestController extends Controller
{
    public function addItem(Request $request)
    {
        try {
            $client = new Client(['cookies' => true]);
            $cookieJar = new CookieJar();

            $response = $client->request('GET', 'https://admin.lashowroom.com/login.php', [
                'cookies' => $cookieJar
            ]);

            $response = $client->request('POST', 'https://admin.lashowroom.com/login.php', [
                'form_params' => [
                    'uname' => 'faveurandmerci',
                    'pwd' => '214e2qyz',
                    'login' => 'Login',
                    'adminLoginLASCheck' => $cookieJar->toArray()[2]['Value'],
                ],
                'cookies' => $cookieJar,
                'allow_redirects' => true,
                'on_stats' => function (TransferStats $stats) use (&$url) {
                    $url = $stats->getEffectiveUri();
                }
            ]);

            if ($url->__toString() != "https://admin.lashowroom.com/login_verify.php") {
                return 'Oops! Wrong credentials.';
            }

            $response = $client->request('POST', 'https://admin.lashowroom.com/login_verify.php', [
                'form_params' => [
                    'v_code' => '5155',
                    'submit' => 'Submit',
                ],
                'cookies' => $cookieJar,
                'allow_redirects' => true,
                'on_stats' => function (TransferStats $stats) use (&$url) {
                    $url = $stats->getEffectiveUri();
                }
            ]);

            if ($url->__toString() != "https://admin.lashowroom.com/index.php") {
                return 'Oops! Wrong key.';
            }

            $response = $client->request('POST', 'https://admin.lashowroom.com/item_add.php', [
                'multipart' => [
                    [
                        'name' => 'launch_date_select',
                        'contents' => ''
                    ],
                    [
                        'name' => 'launch_time_select',
                        'contents' => ''
                    ],
                    [
                        'name' => 'style_no',
                        'contents' => rand(10, 100),
                    ],
                    [
                        'name' => 'category_id',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_category',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_collection',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_pattern',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_length',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style1',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style2',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style3',
                        'contents' => '',
                    ],
                    [
                        'name' => 'assigngroup',
                        'contents' => '',
                    ],
                    [
                        'name' => 'description',
                        'contents' => 'Test Description',
                    ],
                    [
                        'name' => 'min_order',
                        'contents' => '12',
                    ],
                    [
                        'name' => 'price',
                        'contents' => '100.70',
                    ],
                    [
                        'name' => 'time',
                        'contents' => '012314',
                    ],
                    [
                        'name' => 'comment',
                        'contents' => 'Test in packs of 2S, 2M, and 2L per color.',
                    ],
                    [
                        'name' => 'fabric',
                        'contents' => '',
                    ],
                    [
                        'name' => 'content',
                        'contents' => '',
                    ],
                    [
                        'name' => 'origin',
                        'contents' => '',
                    ],
                    [
                        'name' => 'size',
                        'contents' => 'S-M-L',
                    ],
                    [
                        'name' => 'is_plus_size',
                        'contents' => '0',
                    ],
                    [
                        'name' => 'prepack_option',
                        'contents' => '1',
                    ],
                    [
                        'name' => 'prepack_qty',
                        'contents' => '16',
                    ],
                    [
                        'name' => 'color',
                        'contents' => rand(1000, 100000),
                    ],
                    [
                        'name' => 'st_id',
                        'contents' => '1949',
                    ],
                    [
                        'name' => 'image_file_large',
                        'contents' => fopen(public_path('images/1.jpg'), 'r'),
                        'filename' => '1.jpg',
                        'headers' => ['Content-Type' => 'image/jpeg']
                    ],
                    [
                        'name' => 'image_file_large_p',
                        'contents' => '',
                    ],
                    [
                        'name' => 'image_file_large_p',
                        'contents' => '',
                    ],
                    [
                        'name' => 'image_file_detail',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_1',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_2',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_3',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_4',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_5',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_1',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_1',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_2',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_2',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_3',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_3',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_4',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_4',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_5',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_5',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_6',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_6',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_7',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_7',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_8',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_8',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_9',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_9',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_10',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_10',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_11',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_11',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_12',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_12',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_13',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_13',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_14',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_14',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'submit',
                        'contents' => 'Preview',
                    ],
                    [
                        'name' => 'sub_sid',
                        'contents' => '1949',
                    ],
                    [
                        'name' => 'new_style_prepack',
                        'contents' => '0',
                    ],
                ],
                'cookies' => $cookieJar,
                'allow_redirects' => true,
                'on_stats' => function (TransferStats $stats) use (&$url) {
                    $url = $stats->getEffectiveUri();
                }
            ]);

            if ($url->__toString() != "https://admin.lashowroom.com/item_add.php") {
                return 'Oops! Something wrong.';
            }

            $response = $client->request('POST', 'https://admin.lashowroom.com/item_add.php', [
                'multipart' => [
                    [
                        'name' => 'launch_date_select',
                        'contents' => ''
                    ],
                    [
                        'name' => 'launch_time_select',
                        'contents' => ''
                    ],
                    [
                        'name' => 'style_no',
                        'contents' => rand(10, 100),
                    ],
                    [
                        'name' => 'category_id',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_category',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_collection',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_pattern',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_length',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style1',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style2',
                        'contents' => '',
                    ],
                    [
                        'name' => 'las_filter_style3',
                        'contents' => '',
                    ],
                    [
                        'name' => 'assigngroup',
                        'contents' => '',
                    ],
                    [
                        'name' => 'description',
                        'contents' => 'Test Description',
                    ],
                    [
                        'name' => 'min_order',
                        'contents' => '12',
                    ],
                    [
                        'name' => 'price',
                        'contents' => '100.70',
                    ],
                    [
                        'name' => 'time',
                        'contents' => '012314',
                    ],
                    [
                        'name' => 'comment',
                        'contents' => 'Test in packs of 2S, 2M, and 2L per color.',
                    ],
                    [
                        'name' => 'fabric',
                        'contents' => '',
                    ],
                    [
                        'name' => 'content',
                        'contents' => '',
                    ],
                    [
                        'name' => 'origin',
                        'contents' => '',
                    ],
                    [
                        'name' => 'size',
                        'contents' => 'S-M-L',
                    ],
                    [
                        'name' => 'is_plus_size',
                        'contents' => '0',
                    ],
                    [
                        'name' => 'prepack_option',
                        'contents' => '1',
                    ],
                    [
                        'name' => 'prepack_qty',
                        'contents' => '16',
                    ],
                    [
                        'name' => 'color',
                        'contents' => rand(1000, 100000),
                    ],
                    [
                        'name' => 'st_id',
                        'contents' => '1949',
                    ],
                    [
                        'name' => 'image_file_large',
                        'contents' => fopen(public_path('images/1.jpg'), 'r'),
                        'filename' => '1.jpg',
                        'headers' => ['Content-Type' => 'image/jpeg']
                    ],
                    [
                        'name' => 'image_file_large_p',
                        'contents' => '',
                    ],
                    [
                        'name' => 'image_file_large_p',
                        'contents' => '',
                    ],
                    [
                        'name' => 'image_file_detail',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_1',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_2',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_3',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_4',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_detail_file_5',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_1',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_1',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_2',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_2',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_3',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_3',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_4',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_4',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_5',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_5',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_6',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_6',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_7',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_7',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_8',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_8',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_9',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_9',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_10',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_10',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_11',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_11',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_12',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_12',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_13',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_13',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'add_color_name_14',
                        'contents' => '',
                    ],
                    [
                        'name' => 'add_color_file_14',
                        'contents' => '',
                        'filename' => '',
                        'headers' => ['Content-Type' => 'application/octet-stream']
                    ],
                    [
                        'name' => 'submit',
                        'contents' => 'Add Item',
                    ],
                    [
                        'name' => 'sub_sid',
                        'contents' => '1949',
                    ],
                    [
                        'name' => 'new_style_prepack',
                        'contents' => '0',
                    ],
                ],
                'cookies' => $cookieJar,
                'allow_redirects' => true,
                'on_stats' => function (TransferStats $stats) use (&$url) {
                    $url = $stats->getEffectiveUri();
                }
            ]);

            if ($url->__toString() != "https://admin.lashowroom.com/item_add.php?upload=success") {
                return 'Oops! Not added.';
            }
            return $url;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}



