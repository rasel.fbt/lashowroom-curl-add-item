<form method="post" action="item_add.php" id="itemadd_form" name="itemadd_form" enctype="multipart/form-data"
      _lpchecked="1">
    <div id="add_item_load" class="overlay-opacity hide"></div>
    <div class="overlay-opacity-content hide">
        <img src="images/loader_wait.gif" alt="" title="loading this page">
        <div id="view_item_upload_msg">Add Item Loading.....</div>
    </div>
    <table id="maintbl" class="edit-sub">
        <tbody>
        <tr class="header">
            <td colspan="2">
                <div class="floatl">
                    Item Information<br>
                    <span class="required i nb">*Fields in red are required</span></div>
                <div class="floatr mgn-lt">
                    Choose a display schedule date &amp; time:
                    <input type="text" class="nb pad-lt hasDatepicker" name="launch_date_select" id="launch_date_select"
                           size="10" value="2018-08-31">
                    <select name="launch_time_select" id="launch_time_select" class="nb time-select">
                        <option value=""></option>
                        <option value="04" selected="selected">4 AM</option>
                        <option value="05">5 AM</option>
                        <option value="06">6 AM</option>
                        <option value="07">7 AM</option>
                        <option value="08">8 AM</option>
                        <option value="09">9 AM</option>
                        <option value="10">10 AM</option>
                        <option value="11">11 AM</option>
                        <option value="12">12 PM</option>
                        <option value="13">1 PM</option>
                        <option value="14">2 PM</option>
                        <option value="15">3 PM</option>
                        <option value="16">4 PM</option>
                        <option value="17">5 PM</option>
                        <option value="18">6 PM</option>
                        <option value="19">7 PM</option>
                        <option value="20">8 PM</option>
                        <option value="21">9 PM</option>
                        <option value="22">10 PM</option>
                        <option value="23">11 PM</option>
                    </select>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        <tr class="item_add_row">
            <td class="pad-0">
                <table class="geninfo" style="width: 369px;">
                    <tbody>
                    <tr class="header">
                        <td colspan="2">Item Details</td>
                    </tr>
                    <tr>
                        <td style="width:50%;" class="required big">Style No.<span class="required">*</span></td>
                        <td><input type="text" name="style_no" id="style_no" size="20" maxlength="15" value="25412"
                                   class="style_nm_ch"
                                   style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;"><span
                                    class="f10">(15char limit)</span>
                            <div id="style_no_message">Style numbers <span
                                        style="color:#ff0"><strong>MUST</strong></span> be unique. You cannot save
                                changes until you enter a unique style number.
                            </div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td class="b">Category</td>
                        <td>
                            <select name="category_id" id="category_id" class="category_box">
                                <option value="">Choose a Category</option>
                                <optgroup label="Tops">
                                    <option value="10">Tops</option>
                                    <option value="28">Body Suits</option>
                                </optgroup>
                                <optgroup label="Dresses">
                                    <option value="6">Dresses</option>
                                </optgroup>
                                <optgroup label="Bottoms">
                                    <option value="14">Pants</option>
                                    <option value="22">Skirts</option>
                                </optgroup>
                                <optgroup label="Activewear, Yoga &amp; Outdoor">
                                    <option value="18">Sets</option>
                                </optgroup>
                                <optgroup label="Outerwear">
                                    <option value="34">Jackets</option>
                                    <option value="54">Cardigans</option>
                                    <option value="58">Hoodies &amp; Pullovers</option>
                                </optgroup>
                                <optgroup label="Sets">
                                    <option value="38">Short &amp; Pant Sets</option>
                                    <option value="65" selected="selected">Skirt &amp; Dress Sets</option>
                                    <option value="72">Sets</option>
                                </optgroup>
                                <optgroup label="Jumpsuits &amp; Rompers">
                                    <option value="46">Jumpsuits</option>
                                    <option value="50">Rompers</option>
                                </optgroup>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="big vat">LAS Common Area Only</div>
                            <div id="las_common_fields">
                                <div class="f11 red hide" id="las_category_msg">Please Note: If you do not select a LAS
                                    Category below, this item will not be displayed on certain pages in LAS common
                                    areas.
                                </div>
                                <div class="las_common" style="width:145px;">LAS Category</div>
                                <div class="float-l pad-lt-t"><select name="las_category" id="las_cat"
                                                                      class="las_select las_category_box">
                                        <option value="">Choose One</option>
                                        <option value="266" selected="selected">Skirt &amp; Dress Sets</option>
                                        <option value="267">Short &amp; Pant Sets</option>
                                        <option value="268">Skirt Suits</option>
                                        <option value="269">Pant Suits</option>
                                    </select></div>
                                <div class="clear"></div>
                                <div class="las_common" style="width:145px;">LAS Collection</div>
                                <div class="float-l pad-lt-t"><select name="las_collection" id="las_coll"
                                                                      class="las_select  las_collection_box">
                                        <option value="">Choose One</option>
                                        <option value="4">Contemporary</option>
                                        <option value="49">Intimates</option>
                                        <option value="3">Junior</option>
                                        <option value="9">Junior Plus</option>
                                        <option value="7">Maternity</option>
                                        <option value="5">Missy</option>
                                        <option value="10">Missy Plus</option>
                                        <option value="6">Party Dresses</option>
                                        <option value="11">Swimwear</option>
                                        <option value="1" selected="selected">Young Contemporary</option>
                                    </select></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="view_las_common ">
                        <td colspan="2">
                            <div class="big">LAS Buyer Search Options</div>
                            <div class="clearfix pad-lt-t">
                                <div class="float-l" style="width:45%;">
                                    <div class="las_common" style="width:50px;">Pattern</div>
                                    <div class="float-l"><select name="las_filter_pattern" id="las_pat"
                                                                 class="las_select">
                                            <option value="">Choose One</option>
                                            <option value="Crochet/Knit" selected="selected">Crochet/Knit</option>
                                            <option value="Denim">Denim</option>
                                            <option value="Faux Leather">Faux Leather</option>
                                            <option value="Faux Suede">Faux Suede</option>
                                            <option value="Lace">Lace</option>
                                            <option value="Multi-Fabric">Multi-Fabric</option>
                                            <option value="Other">Other</option>
                                            <option value="Plaid">Plaid</option>
                                            <option value="Print">Print</option>
                                            <option value="Solid">Solid</option>
                                            <option value="Striped">Striped</option>
                                            <option value="Textured">Textured</option>
                                        </select></div>
                                    <div class="clear pad-lt-t"></div>
                                    <div class="las_common" style="width:50px;">Length</div>
                                    <div class="float-l"><select name="las_filter_length" id="las_len"
                                                                 class="las_select">
                                            <option value="">Choose One</option>
                                            <option value="Top and Pants Set">Top and Pants Set</option>
                                            <option value="Top and Shorts Set">Top and Shorts Set</option>
                                            <option value="Top and Skirt Set">Top and Skirt Set</option>
                                            <option value="Cropped">Cropped</option>
                                            <option value="Full Length">Full Length</option>
                                            <option value="Dress Set">Dress Set</option>
                                            <option value="Knee-Length">Knee-Length</option>
                                            <option value="Calf-Length" selected="selected">Calf-Length</option>
                                            <option value="Short">Short</option>
                                        </select></div>
                                </div>
                                <div class="float-l">
                                    <div class="las_common" style="width:80px;">Style Option 1</div>
                                    <div class="float-l"><select name="las_filter_style1" id="las_sty1"
                                                                 class="las_select">
                                            <option value="">Choose One</option>
                                            <option value="Belted">Belted</option>
                                            <option value="Embroidered">Embroidered</option>
                                            <option value="Jacket/Skirt">Jacket/Skirt</option>
                                            <option value="Jacket/Dress">Jacket/Dress</option>
                                            <option value="Jacket/Pants">Jacket/Pants</option>
                                            <option value="Sweat Suit">Sweat Suit</option>
                                            <option value="2-Piece">2-Piece</option>
                                            <option value="3-Piece" selected="selected">3-Piece</option>
                                            <option value="Embellished">Embellished</option>
                                            <option value="Sequins">Sequins</option>
                                            <option value="Seamless">Seamless</option>
                                            <option value="Chiffon">Chiffon</option>
                                            <option value="Color Block">Color Block</option>
                                            <option value="Polka Dot">Polka Dot</option>
                                            <option value="Tie Dye">Tie Dye</option>
                                        </select></div>
                                    <div class="clear pad-lt-t"></div>
                                    <div class="las_common" style="width:80px;">Style Option 2</div>
                                    <div class="float-l"><select name="las_filter_style2" id="las_sty2"
                                                                 class="las_select">
                                            <option value="">Choose One</option>
                                            <option value="Belted">Belted</option>
                                            <option value="Embroidered">Embroidered</option>
                                            <option value="Jacket/Skirt">Jacket/Skirt</option>
                                            <option value="Jacket/Dress">Jacket/Dress</option>
                                            <option value="Jacket/Pants">Jacket/Pants</option>
                                            <option value="Sweat Suit" selected="selected">Sweat Suit</option>
                                            <option value="2-Piece">2-Piece</option>
                                            <option value="3-Piece">3-Piece</option>
                                            <option value="Embellished">Embellished</option>
                                            <option value="Sequins">Sequins</option>
                                            <option value="Seamless">Seamless</option>
                                            <option value="Chiffon">Chiffon</option>
                                            <option value="Color Block">Color Block</option>
                                            <option value="Polka Dot">Polka Dot</option>
                                            <option value="Tie Dye">Tie Dye</option>
                                        </select></div>
                                    <div class="clear pad-lt-t"></div>
                                    <div class="las_common" style="width:80px;">Style Option 3</div>
                                    <div class="float-l"><select name="las_filter_style3" id="las_sty3"
                                                                 class="las_select">
                                            <option value="">Choose One</option>
                                            <option value="Belted">Belted</option>
                                            <option value="Embroidered">Embroidered</option>
                                            <option value="Jacket/Skirt">Jacket/Skirt</option>
                                            <option value="Jacket/Dress">Jacket/Dress</option>
                                            <option value="Jacket/Pants">Jacket/Pants</option>
                                            <option value="Sweat Suit">Sweat Suit</option>
                                            <option value="2-Piece">2-Piece</option>
                                            <option value="3-Piece">3-Piece</option>
                                            <option value="Embellished">Embellished</option>
                                            <option value="Sequins" selected="selected">Sequins</option>
                                            <option value="Seamless">Seamless</option>
                                            <option value="Chiffon">Chiffon</option>
                                            <option value="Color Block">Color Block</option>
                                            <option value="Polka Dot">Polka Dot</option>
                                            <option value="Tie Dye">Tie Dye</option>
                                        </select></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="border-n"><input type="hidden" name="assigngroup" id="assigngroup"
                                                                value=""></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="big">Item Description</div>
                            (255 characters maximum)<br>
                            <textarea name="description" id="description"
                                      class="item_desc_comment">*** New Style ***</textarea><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="big">Minimum Order</div>
                        </td>
                        <td>
                            <input type="text" name="min_order" id="min_order" size="2" maxlength="3"
                                   class="cl_name_nnn_num" value="6">
                        </td>
                    </tr>
                    <tr class="odd">
                        <td>
                            <div class="big">Unit Price $</div>
                        </td>
                        <td>
                            <input type="text" name="price" id="price" size="10" maxlength="8" value="12.25">
                            <input type="hidden" name="time" value="111301">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="big">Comments</div>
                            <textarea name="comment" id="comment" class="item_desc_comment">In packs of 2S, 2M, and 2L per color.</textarea>
                            <div class="i pad-b">(You can enter optional information such as prepack size/unit breakdown
                                or special ordering requirements for this item.)
                            </div>
                        </td>
                    </tr>
                    <tr cpass="odd">
                        <td>
                            <div class="big">Fabric</div>
                        </td>
                        <td>
                            <input type="text" name="fabric" id="fabric" size="20" maxlength="255" value="Test Fabric">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="big">Content</div>
                        </td>
                        <td>
                            <input type="text" name="content" id="content" size="20" maxlength="255"
                                   value="Test Content">
                        </td>
                    </tr>
                    <tr class="odd">
                        <td>
                            <div class="big">Origin:</div>
                        </td>
                        <td>
                            <select name="origin" id="origin">
                                <option value="" selected="selected"></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="f11">
                            (Missing Origin? <a href="mailto:support@lashowroom.com?Subject=Origin%20Request"
                                                class="link08">E-mail us your request!</a>)
                        </td>
                    </tr>
                    <tr style="background-color:#384D7B">
                        <td colspan="2">
                            <div class="f12 white b">Size &amp; Prepack Options</div>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td>
                            <div class="big">Sizes</div>
                            <div class="i">(All spaces will be removed.)</div>
                        </td>
                        <td><input type="text" name="size" id="size" size="20" maxlength="35" value="S-M-L"
                                   class="unset-readonly black"></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="big">Is Plus Size?</div>
                        </td>
                        <td>
<span id="ps_opt_0" class="b nowrap set-plussize-option table-radio">
<input type="radio" id="ps_opt_set_0" name="is_plus_size" value="0"><label for="ps_opt_set_0">No</label>
</span>
                            <span id="ps_opt_1"
                                  class="b nowrap set-plussize-option table-radio table-radio-selected-yes">
<input type="radio" id="ps_opt_set_1" name="is_plus_size" value="1" checked="checked"><label
                                        for="ps_opt_set_1">Yes</label>
</span>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td>
                            <div class="big">Prepack?</div>
                        </td>
                        <td>
<span id="pp_opt_0" class="set-prepack-option table-radio">
<input type="radio" id="pp_opt_set_0" name="prepack_option" value="0"><label for="pp_opt_set_0">No</label>
</span>
                            <span id="pp_opt_1" class="set-prepack-option table-radio table-radio-selected-yes">
<input type="radio" id="pp_opt_set_1" name="prepack_option" value="1" checked="checked"><label
                                        for="pp_opt_set_1">Yes</label>
</span>
                        </td>
                    </tr>
                    <tr class="odd display-prepack-qty">
                        <td>
                            <div class="big">Units per Pack</div>
                        </td>
                        <td>
                            <input type="text" name="prepack_qty" id="prepack_qty" size="10" maxlength="3" value="6"
                                   class="cl_name_nnn_num unset-readonly">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="pad-0">
                <table class="geninfo" style="width: 422px;">
                    <tbody>
                    <tr class="header">
                        <td colspan="2">Image Upload</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="editimage">
                                <tbody>
                                <tr>
                                    <th colspan="2">Default Color</th>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="required big">Default Color Name<span class="required">*</span>
                                        </div>
                                    </td>
                                    <td class="">
                                        <input type="text" name="color" id="color"
                                               class="color-name-field ui-autocomplete-input" value="Test Color"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true">
                                        <input type="hidden" name="st_id" id="st_id" value="1949">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="required f11">Color names can only contain letters, numbers, spaces,
                                            slashes ( / ) and hyphens ( - ).
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="b">Default Image<span class="required">*</span></div>
                                    </td>
                                    <td class="">
                                        <input type="hidden" name="testing" id="testing" class="file" value="1">
                                        <input type="hidden" name="test" id="test" class="file" value="">
                                        <a onclick="viewdetail('1949','c','0','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=0&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="image_file_large" id="image_file_large" class="file">
                                        <input type="hidden" name="image_file_large_p" id="image_file_large_p"
                                               class="file" value="/tmp_las/large_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="required f11">Required Minimum Image Size: 400px X 600px; <span
                                                    class="i">300KB maximum file size</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="2">Detail Images</th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="required f11">Required Minimum Image Size: 400px X 600px; <span
                                                    class="i">300KB maximum file size</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Default Detail Image</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','0','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=0&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="image_file_detail" id="image_file_detail" class="file">
                                        <input type="hidden" name="image_file_detail_p" id="image_file_detail_p"
                                               class="file" value="/tmp_las/detail_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="2">Additional Details</th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="required f11">Required Minimum Image Size: 400px X 600px; <span
                                                    class="i">300KB maximum file size</span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Detail 1</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','1','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=1&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="add_detail_file_1" id="add_detail_file_1" class="file">
                                        <input type="hidden" name="add_detail_file_p_1" id="add_detail_file_p_1"
                                               class="file" value="/tmp_las/detail_1_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Detail 2</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','2','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=2&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="add_detail_file_2" id="add_detail_file_2" class="file">
                                        <input type="hidden" name="add_detail_file_p_2" id="add_detail_file_p_2"
                                               class="file" value="/tmp_las/detail_2_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Detail 3</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','3','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=3&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="add_detail_file_3" id="add_detail_file_3" class="file">
                                        <input type="hidden" name="add_detail_file_p_3" id="add_detail_file_p_3"
                                               class="file" value="/tmp_las/detail_3_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Detail 4</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','4','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=4&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="add_detail_file_4" id="add_detail_file_4" class="file">
                                        <input type="hidden" name="add_detail_file_p_4" id="add_detail_file_p_4"
                                               class="file" value="/tmp_las/detail_4_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="big">Detail 5</div>
                                    </td>
                                    <td class="">
                                        <a onclick="viewdetail('1949','d','5','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=d&amp;num=5&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        <input type="file" name="add_detail_file_5" id="add_detail_file_5" class="file">
                                        <input type="hidden" name="add_detail_file_p_5" id="add_detail_file_p_5"
                                               class="file" value="/tmp_las/detail_5_1949_11130116894.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="2">Additional Colors</th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="required f11">Required Minimum Image Size: 400px X 600px; <span
                                                    class="i">300KB maximum file size</span><br>
                                            Color names can only contain letters, numbers, spaces, slashes ( / ) and
                                            hyphens ( - ).
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 1</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_1" id="add_color_name_1"
                                               class="color-name-field ui-autocomplete-input" value="BLUE VIOLET"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','1','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=1&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_1" id="add_color_file_1" class="file">
                                        <input type="hidden" name="add_color_file_p_1" id="add_color_file_p_1"
                                               class="file" value="/tmp_las/color_1_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 2</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_2" id="add_color_name_2"
                                               class="color-name-field ui-autocomplete-input" value="AMBER"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','2','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=2&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_2" id="add_color_file_2" class="file">
                                        <input type="hidden" name="add_color_file_p_2" id="add_color_file_p_2"
                                               class="file" value="/tmp_las/color_2_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 3</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_3" id="add_color_name_3"
                                               class="color-name-field ui-autocomplete-input" value="BISQUE"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','3','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=3&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_3" id="add_color_file_3" class="file">
                                        <input type="hidden" name="add_color_file_p_3" id="add_color_file_p_3"
                                               class="file" value="/tmp_las/color_3_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 4</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_4" id="add_color_name_4"
                                               class="color-name-field ui-autocomplete-input" value="IVORY"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','4','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=4&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_4" id="add_color_file_4" class="file">
                                        <input type="hidden" name="add_color_file_p_4" id="add_color_file_p_4"
                                               class="file" value="/tmp_las/color_4_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 5</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_5" id="add_color_name_5"
                                               class="color-name-field ui-autocomplete-input" value="BLACK"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','5','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=5&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_5" id="add_color_file_5" class="file">
                                        <input type="hidden" name="add_color_file_p_5" id="add_color_file_p_5"
                                               class="file" value="/tmp_las/color_5_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 6</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_6" id="add_color_name_6"
                                               class="color-name-field ui-autocomplete-input" value="ANTIQUE WHITE"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','6','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=6&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_6" id="add_color_file_6" class="file">
                                        <input type="hidden" name="add_color_file_p_6" id="add_color_file_p_6"
                                               class="file" value="/tmp_las/color_6_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 7</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_7" id="add_color_name_7"
                                               class="color-name-field ui-autocomplete-input" value="COBALT"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','7','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=7&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_7" id="add_color_file_7" class="file">
                                        <input type="hidden" name="add_color_file_p_7" id="add_color_file_p_7"
                                               class="file" value="/tmp_las/color_7_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 8</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_8" id="add_color_name_8"
                                               class="color-name-field ui-autocomplete-input" value="FLORAL WHITE"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','8','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=8&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_8" id="add_color_file_8" class="file">
                                        <input type="hidden" name="add_color_file_p_8" id="add_color_file_p_8"
                                               class="file" value="/tmp_las/color_8_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 9</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_9" id="add_color_name_9"
                                               class="color-name-field ui-autocomplete-input" value="NAVAJO WHITE"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','9','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=9&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_9" id="add_color_file_9" class="file">
                                        <input type="hidden" name="add_color_file_p_9" id="add_color_file_p_9"
                                               class="file" value="/tmp_las/color_9_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 10</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_10" id="add_color_name_10"
                                               class="color-name-field ui-autocomplete-input" value="BLUSH"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','10','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=10&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_10" id="add_color_file_10" class="file">
                                        <input type="hidden" name="add_color_file_p_10" id="add_color_file_p_10"
                                               class="file" value="/tmp_las/color_10_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 11</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_11" id="add_color_name_11"
                                               class="color-name-field ui-autocomplete-input" value="BROWN"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','11','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=11&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_11" id="add_color_file_11" class="file">
                                        <input type="hidden" name="add_color_file_p_11" id="add_color_file_p_11"
                                               class="file" value="/tmp_las/color_11_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 12</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_12" id="add_color_name_12"
                                               class="color-name-field ui-autocomplete-input" value="DARK GRAY"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','12','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=12&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_12" id="add_color_file_12" class="file">
                                        <input type="hidden" name="add_color_file_p_12" id="add_color_file_p_12"
                                               class="file" value="/tmp_las/color_12_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 13</td>
                                </tr>
                                <tr>
                                    <td class="odd">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_13" id="add_color_name_13"
                                               class="color-name-field ui-autocomplete-input" value="CARROT"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="odd">
                                        <a onclick="viewdetail('1949','c','13','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=13&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_13" id="add_color_file_13" class="file">
                                        <input type="hidden" name="add_color_file_p_13" id="add_color_file_p_13"
                                               class="file" value="/tmp_las/color_13_1949_11130116894.jpg"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="head big">Color 14</td>
                                </tr>
                                <tr>
                                    <td class="even">
                                        <div>Color Name</div>
                                        <input type="text" name="add_color_name_14" id="add_color_name_14"
                                               class="color-name-field ui-autocomplete-input" value="ROYAL BLUE"
                                               autocomplete="off" role="textbox" aria-autocomplete="list"
                                               aria-haspopup="true"></td>
                                    <td class="even">
                                        <a onclick="viewdetail('1949','c','14','11130116894')"
                                           href="javascript:void(0);"><img
                                                    src="imgview.php?stid=1949&amp;clr=c&amp;num=14&amp;tm=11130116894"
                                                    width="50"></a><br>
                                        Upload Image<br>
                                        <input type="file" name="add_color_file_14" id="add_color_file_14" class="file">
                                        <input type="hidden" name="add_color_file_p_14" id="add_color_file_p_14"
                                               class="file" value="/tmp_las/color_14_1949_11130116894.jpg"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="item_add_row">
            <td colspan="2" class="tac border vam pad">
                <input type="submit" name="submit" value="Preview" class="btn submit_post">
                <input type="submit" name="submit" value="Add Item" class="btn submit_post" id="submit_post_add">
                <input type="submit" name="submit" value="Add Item" class="submit_post_disable" id="submit_post_disable"
                       style="display:none;" disabled="disabled">
                <input type="submit" name="reset" value="Cancel" class="btn">
            </td>
        </tr>
        </tbody>
    </table>
    <input type="hidden" name="sub_sid" id="sub_sid" value="1949">
    <input type="hidden" name="new_style_prepack" id="new_style_prepack" value="0">
</form>